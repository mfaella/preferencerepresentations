#!/bin/bash

sed 's/(or/(or\n/' | grep "(and" | awk '{ print $5, $NF }' | sed 's/)//g' | awk 'BEGIN { print "digraph Pref {" } { print $1, " -> ", $2, ";" } END { print "}"}' | sed 's/#//g'
