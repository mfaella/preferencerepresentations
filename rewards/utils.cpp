#include <ppl.hh>

#include "utils.h"

using namespace Parma_Polyhedra_Library::IO_Operators;

Constraint negate_constraint(const Constraint& c) {
#if PPL_VERSION_MAJOR == 0
  Linear_Expression e = Linear_Expression(c);
#else
  Linear_Expression e(c.expression());
#endif

  if (c.is_strict_inequality()) {
    return e <= 0;
  } else if (c.is_equality()) {
    cerr << "Cannot negate an equality constraint." << endl;
    exit(1);
  } else { // non-strict inequality
    return e < 0;
  }
}

Constraint strongly_negate_constraint(const Constraint& c) {
#if PPL_VERSION_MAJOR == 0
  Linear_Expression e = Linear_Expression(c);
#else
  Linear_Expression e(c.expression());
#endif

  if (c.is_strict_inequality()) {
    return e < 0;
  } else if (c.is_equality()) {
    cerr << "Cannot negate an equality constraint." << endl;
    exit(1);
  } else { // non-strict inequality
    return e < 0;
  }
}

void insert_all(Constraint_System &sys, const vector<Constraint> &constraints) {
  for (auto c=constraints.begin(); c!=constraints.end(); c++) {
      sys.insert(*c);
  }
}



void print_a_point(const Polyhedron& p) {
  if (p.is_empty()) {
    cout << "Polyhedron is empty." << endl;
    return;
  }
  Generator_System sys = p.generators();
  for (auto gen: sys) {
    if (gen.is_point()) {
      cout << "Witness: " << gen << endl;
      return;
    }
  }
}


void check_emptiness(Constraint_System &sys) {
  Poly p(sys);
  if (p.is_empty()) {
    cout << "Constraints unfeasible." << endl;
  } else {
    cout << "Constraints feasible." << endl;
    print_a_point(p);
  }
}
