#include <ppl.hh>
#include <fstream>

#include "utils.h"

using namespace Parma_Polyhedra_Library;
using namespace Parma_Polyhedra_Library::IO_Operators;
using namespace std;

void check_non_implication(vector<Constraint> premise, 
                           vector<Constraint> conclusion) {
  Constraint_System sys;
  insert_all(sys, premise);
  for (Constraint c: conclusion) {
    Constraint_System non_impl(sys);
    Constraint not_c = negate_constraint(c);
    // cout << "c: " << c << endl;
    // cout << "not c: " << not_c << endl;
    non_impl.insert(not_c);
    Poly p(non_impl);    
    if (!p.is_empty()) {
      cout << "Implication is false. Violated clause " << c << endl;
      print_a_point(p);
      return;
    }
  }
  cout << "Implication is true." << endl;
}


Constraint p_above(const vector<Variable> &vars, int i) {

  if (i<1 || i>=vars.size()) {
    cerr << "p_above index out of range" << endl;
    exit(1);
  }
  
  Linear_Expression expr;
  
  for (int j=i-1; j<vars.size(); j++)
    expr = expr + vars[j];
  
  return Constraint((vars.size() -i+1) * vars[i] < expr);
}


/** Checks representability of the preference on 3 agents where
    c<a<b is better than a=b=c (for a)
    This preference satisfies Dom and SDomI.
*/
void check_3agent_representability() {

  cout << "Checking specific preference on 3 agents." << endl;
  /* Interpretation of variables:

     x/A|a     z/C|a   v/F|a,b
     y/B|b,c   t/D|b   w/G|c
               u/E|c
   */  
  Variable x(0), y(1), z(2), t(3), u(4), v(5), w(6);

  // Well-formedness
  vector<Constraint> wf = { x + 2*y == 1, z+t+u==1, 2*v+w==1,
			    x>0, y>0, z>0, t>0, u>0, v>0, w>0,
                            z>=t, t>=u, x>=y, v>=w };
  // SDomI (more than SSCI)
  vector<Constraint> sdomi = { t>y, v>y, z>y, y>u, t>u, z>u, v>u, v>t, z>t, 1>3*w, x>w, // same as ssci
                              t>w, y>w, x>u, y>u, x>t, v>t, 1>3*y };
  // Dom (more than DomI)
  // Equalities are expressed by inequalities to ease negation
  vector<Constraint> dom = { 
    t>=y, v>=y, z>=y, y>=u, t>=u, z>=u, v>=u, v>=t, z>=t, 1>=3*w, x>=w, // same as sci
    t>=w, y>=w, x>=u, y>=u, x>=t, v>=t, 1>=3*y, // same as DomI
    x>=z, z>=x, w>=u, u>=w // equalities specific to Dom 
  };
  Constraint special = 3*t > 1;
  Constraint_System sys;
  sys.insert(special);
  insert_all(sys, wf);
  insert_all(sys, sdomi);
  insert_all(sys, dom);
  check_emptiness(sys);
}


/** Checks representability of the preference similar to P^{above,equals},
    except for its last "layer", where it is oriented in the opposite direction.
*/
void check_representability(int n_agents) {
  vector<Variable> vars;
  for (int i=0; i<n_agents; i++)
    vars.push_back(Variable(i));

  Constraint_System sys;
  // Add basic constraints for strictly LA rewards
  for (int i=0; i<n_agents; i++) {
    sys.insert(Constraint(vars[i] >= 0));
    if (i>0)
      sys.insert(Constraint(vars[i-1] > vars[i]));
  }
  
  // Add constraints consistent with P^above
  for (int i=1; i<n_agents-2; i++)
    sys.insert(p_above(vars, i));
  
  // Add constraints opposite to P^above
  Constraint last = p_above(vars, n_agents-2);
  Constraint not_last = strongly_negate_constraint(last);
  sys.insert(not_last);

  // DEBUG
  cout << sys << endl;
  
  check_emptiness(sys);
}

/** Checks representability of the preference similar to P^{above,equals},
    except that even "layers" of preference are reversed.
*/
void check_zebra_representability(int n_agents) {
  vector<Variable> vars;
  for (int i=0; i<n_agents; i++)
    vars.push_back(Variable(i));

  Constraint_System sys;
  // Add basic constraints for strictly LA rewards
  for (int i=0; i<n_agents; i++) {
    sys.insert(Constraint(vars[i] >= 0));
    if (i>0)
      sys.insert(Constraint(vars[i-1] > vars[i]));
  }
  
  // Add constraints consistent with P^above
  for (int i=1; i<n_agents-1; i+=2)
    sys.insert(p_above(vars, i));
  
  // Add constraints opposite to P^above
  for (int i=2; i<n_agents-1; i+=2) {
    Constraint temp = p_above(vars, i);
    Constraint not_temp = strongly_negate_constraint(temp);
    sys.insert(not_temp);
  }

  // DEBUG
  cout << sys << endl;
  check_emptiness(sys);
}


void check_representability() {
  check_3agent_representability();
  for (int n=4; n<=5; n++) // we checked up to 25
    check_zebra_representability(n);  
}


int main(void) {
  /* Interpretation of variables:

     x/A|a     z/C|a   v/F|a,b
     y/B|b,c   t/D|b   w/G|c
               u/E|c
   */  
  Variable x(0), y(1), z(2), t(3), u(4), v(5), w(6);

  // Well-formedness
  vector<Constraint> wf = { x + 2*y == 1, z+t+u==1, 2*v+w==1,
			    x>0, y>0, z>0, t>0, u>0, v>0, w>0,
                            z>=t, t>=u, x>=y, v>=w };
  // Swap (implied by WF)
  vector<Constraint> sw = { z>=t, t>=u, x>=y, v>=w };
  // Strong Swap
  vector<Constraint> ssw = { z>t, t>u, x>y, v>w };
  // SCI
  vector<Constraint> sci = { t>=y, v>=y, z>=y, y>=u, t>=u, z>=u, v>=u, v>=t, z>=t, 1>=3*w, x>=w };
  // Strong SCI
  vector<Constraint> ssci = { t>y, v>y, z>y, y>u, t>u, z>u, v>u, v>t, z>t, 1>3*w, x>w };
  // DomI (more than SCI)
  vector<Constraint> domi = { t>=y, v>=y, z>=y, y>=u, t>=u, z>=u, v>=u, v>=t, z>=t, 1>=3*w, x>=w, // same as sci
                              t>=w, y>=w, x>=u, y>=u, x>=t, v>=t, 1>=3*y };
  // SDomI (more than SSCI)
  vector<Constraint> sdomi = { t>y, v>y, z>y, y>u, t>u, z>u, v>u, v>t, z>t, 1>3*w, x>w, // same as ssci
                              t>w, y>w, x>u, y>u, x>t, v>t, 1>3*y };
  // Dom (more than DomI)
  // Equalities are expressed by inequalities to ease negation
  vector<Constraint> dom = { 
    t>=y, v>=y, z>=y, y>=u, t>=u, z>=u, v>=u, v>=t, z>=t, 1>=3*w, x>=w, // same as sci
    t>=w, y>=w, x>=u, y>=u, x>=t, v>=t, 1>=3*y, // same as DomI
    x>=z, z>=x, w>=u, u>=w // equalities specific to Dom 
  };

  cout << endl << "SSw and SSCI implies DomI?" << endl;
  vector<Constraint> premise(wf);
  premise.insert(premise.end(), ssw.begin(), ssw.end());
  premise.insert(premise.end(), ssci.begin(), ssci.end());
  check_non_implication(premise, domi);

  cout << endl << "Sw and SSCI implies SSw?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), ssci.begin(), ssci.end());
  check_non_implication(premise, ssw);

  cout << endl << "SSw and SCI implies SSCI?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), sci.begin(), sci.end());
  premise.insert(premise.end(), ssw.begin(), ssw.end());
  check_non_implication(premise, ssci);

  cout << endl << "DomI and SSw implies SSCI?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), ssw.begin(), ssw.end());
  premise.insert(premise.end(), domi.begin(), domi.end());
  check_non_implication(premise, ssci);

  cout << endl << "DomI and SSCI imply SDomI?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), domi.begin(), domi.end());
  premise.insert(premise.end(), ssci.begin(), ssci.end());
  check_non_implication(premise, sdomi);

  cout << endl << "Single axioms" << endl;

  cout << endl << "SDomI implies Dom?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), sdomi.begin(), sdomi.end());
  check_non_implication(premise, dom);

  cout << endl << "Dom implies SDomI?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), dom.begin(), dom.end());
  check_non_implication(premise, sdomi);

  cout << endl << "Dom implies SSW?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), dom.begin(), dom.end());
  check_non_implication(premise, ssw);
  
  cout << endl << "DomI implies SSW? (redundant because even Dom does not imply SSW)" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), domi.begin(), domi.end());
  check_non_implication(premise, ssw);
  
  cout << endl << "Dom implies SSCI?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), dom.begin(), dom.end());
  check_non_implication(premise, ssci);

  // True implications
  cout << endl << "Dom and SSW implies SSCI?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), dom.begin(), dom.end());
  premise.insert(premise.end(), ssw.begin(), ssw.end());
  check_non_implication(premise, ssci);

  cout << endl << "Dom and SSW implies SDomI?" << endl;
  premise.clear();
  premise.insert(premise.end(), wf.begin(), wf.end());
  premise.insert(premise.end(), dom.begin(), dom.end());
  premise.insert(premise.end(), ssw.begin(), ssw.end());
  check_non_implication(premise, sdomi);
}

